<div class="<?php Layout::classes('banner banner-default'); ?>" style="<?php Layout::partial('background'); ?>">
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <div class="flex">
            <div class="inner">
                <?php Field::html('sub_title', '<div class="sub-title"><p>%s</p></div>'); ?>
                <?php Field::html('banner_title', '<h1>%s</h1>'); ?>
            </div>
            <div class="accent">
                <?php Field::image('accent_image'); ?>
            </div>
        </div>
    </div>
</div>
