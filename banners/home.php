<div class="<?php Layout::classes('banner banner-home'); ?>" style="<?php Layout::partial('background'); ?>">
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container content-container">
        <div class="flex">
            <div class="inner">
                <?php Field::html('banner_title', '<h1>%s</h1>'); ?>
                <?php Field::html('banner_desc', '<div class="desc">%s</div>') ?>
                <?php Layout::partial('buttons') ?>
            </div>
            <div class="accent">
                <?php Field::image('accent_image'); ?>
            </div>
        </div>
    </div>
    <?php if (Field::exists('service_icons')) : ?>
        <div class="container icon-container">
            <div class="service-icons">
                <?php foreach (Field::iterable('service_icons') as $loop) : ?>
                    <?php if (Field::anyExist('link', 'link_file')) : ?>
                        <div class="icon">
                            <a href="<?php Layout::partial('link'); ?>">
                                <?php Field::image('icon'); ?>
                                <?php Field::html('label', '<p class="label">%s</p>'); ?>
                            </a>
                        </div>
                    <?php else : ?>
                        <div class="icon">
                            <?php Field::image('icon'); ?>
                            <?php Field::html('label', '<p class="label">%s</p>'); ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
