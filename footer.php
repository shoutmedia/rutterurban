    <footer class="footer" role="contentinfo">
        <div class="container">
            <div class="logos main-logo">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo-alt.png" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">
            </div>
            <div class="inner">
                <div class="col">
                    <p class="heading"><?php echo get_field('summary_title', 'option'); ?></p>
                    <?php echo get_field('summary_text', 'option'); ?>
                </div>
                <div class="col">
                    <p class="heading"><?php echo get_menu_name_from_location('secondary'); ?></p>
                    <nav class="nav" aria-label="site">
                        <?php wp_nav_menu([
                            'theme_location' => 'secondary',
                            'depth' => 1,
                        ]); ?>
                    </nav>
                </div>
                <div class="col">
                    <p class="heading"><?php _e('Call or email us for a free estimate', DOMAIN); ?></p>
                    <div itemscope itemtype="http://schema.org/LocalBusiness" class="address">
                        <?php if (get_field('phone_numbers', 'option')) :
                            foreach (get_field('phone_numbers', 'option') as $phone) :
                                if (!$phone['default']) :
                                    continue;
                                endif; ?>
                                <p><em class="fas fa-phone"></em><a href="tel:<?php echo preg_replace('/\D/', '', $phone['number']); ?>" itemprop="<?php echo $phone['type'] == 'faxNumber' ? $phone['type'] : 'telephone'; ?>"><?php echo $phone['number']; ?></a></p>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        <?php if (get_field('email', 'option')) : ?>
                            <p><em class="fas fa-envelope"></em><a href="mailto:<?php echo get_field('email', 'option'); ?>" itemprop="email"><?php echo get_field('email', 'option'); ?></a></p>
                        <?php endif; ?>
                    </div>
                    <div class="social">
                        <?php Layout::partial('social'); ?>
                    </div>
                </div>
            </div>
            <div class="logos">
                <?php foreach (get_field('logos', 'option') as $logo) : ?>
                    <?php $logo = wp_get_attachment($logo['logo']); ?>
                    <?php if ($logo['link']) : ?>
                        <a href="<?php echo $logo['link']; ?>">
                            <img src="<?php echo $logo['src']; ?>" alt="<?php echo $logo['alt']; ?>">
                        </a>
                    <?php else : ?>
                        <img src="<?php echo $logo['src']; ?>" alt="<?php echo $logo['alt']; ?>">
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </footer>

    <div id="footer-credit" class="<?php the_field('site_credit_style', 'option'); ?>"></div>

    <?php wp_footer(); ?>

</body>
</html>
