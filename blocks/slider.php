<div class="<?php Layout::classes('slider'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>

    <?php foreach (Field::iterable('slides') as $loop) : ?>
        <div class="slide">
            <div class="container">
                <?php if (Field::exists('accent_image')) : ?>
                    <div class="accent">
                        <div class="img">
                            <?php Field::image('accent_image', 'large'); ?>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="info">
                    <?php Layout::flexible(Field::get('content', []), 'components'); ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
