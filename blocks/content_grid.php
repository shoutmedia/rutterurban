<?php $focal = Field::get('focal_point'); ?>
<div class="<?php Layout::classes('content-grid'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php if (Field::exists('items')) : ?>
            <div class="items <?php Field::html('items_per_row', 'x%s', 'x3'); ?>">
                <?php foreach (Field::iterable('items') as $loop) : ?>
                    <div class="item">
                        <?php if (Field::exists('image')) : ?>
                            <?php if ($focal === 'photo') : ?>
                                <?php Field::image('image', 'large'); ?>
                            <?php else : ?>
                                <div class="bg" style="background-image: url(<?php echo Field::src('image', 'large'); ?>)"></div>
                            <?php endif; ?>
                        <?php endif; ?>
                        <div class="text">
                            <?php Field::html('label', '<p class="label">%s</p>'); ?>
                            <?php Field::display('description'); ?>
                            <?php Layout::partial('button'); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
