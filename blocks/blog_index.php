<div class="posts-listing">
    <div class="container">
        <div class="posts">
            <?php if (have_posts()) :
                while (have_posts()) :
                    the_post(); ?>

                    <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
                        <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                        <?php the_excerpt(); ?>
                        <p class="read-more">
                            <a href="<?php the_permalink(); ?>">
                                <span class="btn-label"><?php _e('Read Article', DOMAIN); ?></span>
                                <em class="fas fa-angle-right"></em>
                            </a>
                        </p>
                    </article>

                <?php endwhile; ?>
            <?php else : ?>
                <h2><?php _e('Blog empty', DOMAIN); ?></h2>
                <p><?php _e('There currently aren\'t any posts published.', DOMAIN); ?></p>
            <?php endif; ?>
        </div>

        <div class="pagination">
            <?php echo get_previous_posts_link('Newer Articles'); ?>
            <?php echo get_next_posts_link('Older Articles'); ?>
        </div>
    </div>
</div>
