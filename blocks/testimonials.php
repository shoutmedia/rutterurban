<div class="<?php Layout::classes('testimonials'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <div class="intro">
            <?php Layout::flexible(Field::get('content', []), 'components'); ?>
        </div>
        <div class="testimonial-slider">
            <?php foreach (Field::relationship('testimonials') as $loop) : ?>
                <div class="testimonial">
                    <div class="inner">
                        <p class="label"><?php the_title(); ?></p>
                        <?php Field::html('services_performed', '<p class="services">%s</p>'); ?>
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
