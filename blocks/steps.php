<div class="<?php Layout::classes('steps'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php foreach (Field::iterable('steps') as $loop) : ?>
            <?php if (Field::anyExist('link', 'link_file')) : ?>
                <a href="<?php Layout::partial('link'); ?>" class="step">
                    <div class="count"><?php echo str_pad($loop->index + 1, 2, '0', STR_PAD_LEFT); ?></div>
                    <?php Field::html('label', '<p class="label">%s</p>'); ?>
                </a>
            <?php else : ?>
                <div class="step">
                    <div class="count"><?php echo str_pad($loop->index + 1, 2, '0', STR_PAD_LEFT); ?></div>
                    <?php Field::html('label', '<p class="label">%s</p>'); ?>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>
