<?php

if (Field::exists('btn_label')) :
    $fields = [
        'class' => 'btn',
        'label' => '<span class="btn-label">' . Field::get('btn_label') . '</span>',
    ];

    if (Field::exists('btn_icon')) :
        $fields['before'] = '<em class="fas fa-' . Field::get('btn_icon') . '"></em>';
        $fields['class'] .= ' icon-' . Field::get('btn_icon_placement');
    else :
        $fields['before'] = '<em class="fas fa-angle-right"></em>';
        $fields['class'] .= ' icon-after';
    endif;

    Layout::partial('link', $fields);
endif;
