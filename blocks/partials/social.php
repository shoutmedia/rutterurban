<?php if (get_field('social_platforms', 'option')) : ?>
    <?php Field::display('start'); ?>
    <?php foreach (get_field('social_platforms', 'option') as $option) : ?>
        <?php Field::display('before'); ?><a href="<?php echo $option['url']; ?>" target="_blank" rel="noopener"><em class="fab fa-<?php echo $option['icon']; ?>" aria-hidden="true"></em></a><?php Field::display('after'); ?>
    <?php endforeach; ?>
    <?php Field::display('end'); ?>
<?php endif; ?>
