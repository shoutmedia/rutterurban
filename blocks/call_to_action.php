<div class="<?php Layout::classes('call-to-action'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php Layout::partial('title'); ?>

        <div class="btn-container">
            <?php Layout::partial('button'); ?>
        </div>

        <div class="contact-info">
            <?php if (get_field('phone_numbers', 'option')) : ?>
                <div class="phone">
                    <?php foreach (get_field('phone_numbers', 'option') as $phone) :
                        if (!$phone['default']) :
                            continue;
                        endif; ?>
                        <p class="label"><?php _e('By Phone', DOMAIN); ?></p>
                        <p class="contact"><a href="tel:<?php echo preg_replace('/[^0-9]/', '', $phone['number']); ?>"><?php echo $phone['number']; ?></a></p>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <?php if (get_field('email', 'option')) : ?>
                <div class="email">
                    <p class="label"><?php _e('By Email', DOMAIN); ?></p>
                    <p class="contact"><a href="mailto:<?php echo get_field('email', 'option'); ?>"><?php echo get_field('email', 'option'); ?></a></p>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
