<div class="accordion-wrapper">
    <div class="accordion">
        <?php foreach (Field::iterable('panels') as $loop) : ?>
            <div class="pane">
                <div class="heading" role="button" tabindex="0">
                    <?php Field::html('label', '<span>%s</span>'); ?>
                    <em class="fas fa-long-arrow-alt-right"></em>
                </div>
                <div class="body"><?php Field::display('body'); ?></div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php if (Field::exists('continue_reading_label')) : ?>
        <a href="<?php Field::display('continue_link'); ?>" class="continue">
            <?php Field::html('continue_reading_label', '<span>%s</span>'); ?>
            <em class="fas fa-angle-right"></em>
        </a>
    <?php endif; ?>
</div>
