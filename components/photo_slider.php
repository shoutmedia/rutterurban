<div class="photo-slider">
    <div class="slider-wrapper">
        <div class="slides">
            <?php foreach (Field::iterable('photos') as $loop) : ?>
                <div class="slide" style="background-image: url(<?php echo Field::src('id', 'large'); ?>)" <?php Field::html('alt', 'aria-label="%s"'); ?> role="img"></div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="information">
        <?php Field::display('information'); ?>
    </div>
</div>
