<?php if (Field::exists('links')) : ?>
    <div class="links">
        <nav aria-label="page">
            <ul class="link-items">
                <?php foreach (Field::iterable('links') as $loop) : ?>
                    <?php if (Field::equals('type', 'heading')) : ?>
                        <li class="heading"><?php Field::display('label'); ?></li>
                    <?php else : ?>
                        <li class="link-item"><?php Layout::partial('link'); ?></li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </nav>
    </div>
<?php endif; ?>
