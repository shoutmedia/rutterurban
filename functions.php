<?php

use Arcadia\Theme\PostTypes;

// Development Environment (local, staging or production)
define('ENV', 'local');

// Translation string domain
define('DOMAIN', 'arcadia');

// Current directory
define('BASE', dirname(__FILE__));

// Google Maps API Key
define('GOOGLEAPI', 'AIzaSyDivzhX9fUCCqvaJWhbVXu-y1EDBCycwuU');

// Helper functions
require_once(BASE . '/inc/helpers.php');

// Autoloader
require_once(BASE . '/inc/autoloader.php');

// Load Arcadia
\Arcadia\Setup::init();

/**
 * Current Theme Setup
 */
function theme_setup()
{
    // Thumbnails
    add_theme_support('post-thumbnails');

    // Title Tag
    add_theme_support('title-tag');

    // Menus
    register_nav_menu('primary', __('Navigation Menu', DOMAIN));
    register_nav_menu('secondary', __('Footer Menu', DOMAIN));
    register_nav_menu('mobile', __('Mobile Menu', DOMAIN));

    // When inserting an image don't link it
    update_option('image_default_link_type', 'none');

    // Remove Gallery Styling
    add_filter('use_default_gallery_style', '__return_false');

    // Additional Image Sizes
    add_image_size('banner', 1920, 1920);
}

add_action('after_setup_theme', 'theme_setup');

/**
 * Load theme specific assets
 */
function scripts_styles()
{
    // Load Stylesheets
    wp_enqueue_style(DOMAIN . '-slick', get_template_directory_uri() . '/slick/slick.min.css');
    wp_enqueue_style(DOMAIN . '-slick-theme', get_template_directory_uri() . '/slick/accessible-slick-theme.min.css');
    wp_enqueue_style(DOMAIN . '-style', get_stylesheet_uri());

    // Google Maps
    wp_register_script(DOMAIN . '-gmaps', 'https://maps.googleapis.com/maps/api/js?key=' . GOOGLEAPI, [], null, true);
    wp_register_script(DOMAIN . '-cluster', get_template_directory_uri() . '/js/markerclusterer.js', [], null, true);

    if (Layout::containsBlock('map')) {
        wp_enqueue_script(DOMAIN . '-gmaps');
        wp_enqueue_script(DOMAIN . '-cluster');
    }

    // Load JavaScript
    wp_enqueue_script(DOMAIN . '-jquery', '//code.jquery.com/jquery-1.11.0.min.js', [], null);
    wp_enqueue_script(DOMAIN . '-slick', get_template_directory_uri() . '/slick/slick.min.js', ['jquery'], null, true);
    wp_enqueue_script(DOMAIN . '-polyfill', 'https://cdnjs.cloudflare.com/polyfill/v3/polyfill.min.js', [], null);
    wp_enqueue_script(DOMAIN . '-main', get_template_directory_uri() . '/js/main.js', [], null, true);
}

add_action('wp_enqueue_scripts', 'scripts_styles');

/**
 * Custom Editor Styles
 * @param  array $init TinyMCE Options
 * @return array       Updated TinyMCE Options
 */
function editor_style_options($init)
{
    $style_formats = [
        [
            'title' => 'Small Text',
            'selector' => '*',
            'classes' => 'text-small',
        ],
        [
            'title' => 'Medium Text',
            'selector' => '*',
            'classes' => 'text-medium',
        ],
        [
            'title' => 'Large Text',
            'selector' => '*',
            'classes' => 'text-large',
        ],
        [
            'title' => 'Extra Large Text',
            'selector' => '*',
            'classes' => 'text-xlarge',
        ],
        [
            'title' => 'Button',
            'selector' => 'a',
            'classes' => 'btn',
        ],
    ];

    $init['style_formats'] = json_encode($style_formats);

    return $init;
}

add_filter('tiny_mce_before_init', 'editor_style_options');

/**
 * ACF Maps Key
 */
function my_acf_init()
{
    acf_update_setting('google_api_key', GOOGLEAPI);
}

add_action('acf/init', 'my_acf_init');

function create_post_types()
{
    PostTypes::genericPostType('testimonial', 'Testimonial', 'Testimonials', [
        'menu_position' => 55,
        'supports' => [
            'title',
            'editor',
        ],
    ]);
}

add_action('init', 'create_post_types');
